﻿namespace ItaiTzur.utilities.hebrew_calendar_generator
{
    using ItaiTzur.utilities.hebrew_calendar_generator.Interfaces;
    using System.IO;
    using System.Security.AccessControl;

    internal sealed class DirectoryWrapper : IDirectoryOperator
    {
        /// <summary>
        /// Gets a <see cref="DirectorySecurity"/> object that encapsulates the access control list (ACL) entries for a
        /// specified directory.
        /// </summary>
        /// <param name="path">
        /// The path to a directory containing a DirectorySecurity object that describes the file's access control list
        /// (ACL) information.
        /// </param>
        /// <returns>
        /// An object that encapsulates the access control rules for the file described by the <paramref name="path"/>
        /// parameter.
        /// </returns>
        public DirectorySecurity GetAccessControl(string path)
        {
            return Directory.GetAccessControl(path);
        }
    }
}
