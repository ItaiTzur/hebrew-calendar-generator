﻿namespace ItaiTzur.utilities.hebrew_calendar_generator
{
    using ItaiTzur.utilities.hebrew_calendar_generator.Entities.Contracts;
    using ItaiTzur.utilities.hebrew_calendar_generator.Interfaces;
    using System.Collections.Generic;
    using System.IO;
    using YamlDotNet.Serialization;
    using YamlDotNet.Serialization.NamingConventions;

    internal sealed class HolidayRepository : IHolidayRepository
    {
        private readonly HolidayContract[] _holidays;

        internal HolidayRepository(string path)
        {
            var deserializer =
                new DeserializerBuilder().WithNamingConvention(PascalCaseNamingConvention.Instance).Build();
            using (var reader = new StreamReader(path))
            {
                _holidays = deserializer.Deserialize<HolidayContract[]>(reader);
            }
        }

        public IEnumerable<HolidayContract> GetAllHolidays()
        {
            return _holidays;
        }
    }
}
