﻿namespace ItaiTzur.utilities.hebrew_calendar_generator
{
    using ItaiTzur.utilities.hebrew_calendar_generator.Entities;
    using ItaiTzur.utilities.hebrew_calendar_generator.Interfaces;
    using ItaiTzur.utilities.hebrew_calendar_generator.Interfaces.Excel;
    using Microsoft.Office.Interop.Excel;
    using System;
    using System.IO;
    using System.Runtime.InteropServices;

    internal sealed class HebrewCalendarGenerator : IHebrewCalendarGenerator
    {
        private readonly IApplicationFactory _applicationFactory;
        private readonly IConsoleOperator _consoleOperator;
        private readonly IDirectoryOperator _directoryOperator;
        private readonly IFileOperator _fileOperator;
        private readonly IHebrewDateCalculator _hebrewDateCalculator;
        private readonly IPathOperator _pathOperator;
        private readonly IExcelCalendarFactory _excelCalendarFactory;
        private readonly IAssemblyOperator _assemblyOperator;

        public HebrewCalendarGenerator(
            IApplicationFactory applicationFactory,
            IConsoleOperator consoleOperator,
            IDirectoryOperator directoryOperator,
            IFileOperator fileOperator,
            IHebrewDateCalculator hebrewDateCalculator,
            IPathOperator pathOperator,
            IExcelCalendarFactory excelCalendarFactory,
            IAssemblyOperator assemblyOperator)
        {
            _applicationFactory = applicationFactory;
            _consoleOperator = consoleOperator;
            _directoryOperator = directoryOperator;
            _fileOperator = fileOperator;
            _hebrewDateCalculator = hebrewDateCalculator;
            _pathOperator = pathOperator;
            _excelCalendarFactory = excelCalendarFactory;
            _assemblyOperator = assemblyOperator;
        }

        private bool ReadFile(string path, ref string font, PersonalGregorianEventList personalGregorianEvents)
        {
            string[] allPersonalDateLines;
            try
            {
                allPersonalDateLines = _fileOperator.ReadAllLines(path);
            }
            catch (FileNotFoundException)
            {
                _consoleOperator.WriteLine($"{path} unreadable");
                return false;
            }

            var readFile = _hebrewDateCalculator.ReadFile(allPersonalDateLines, ref font, personalGregorianEvents);
            if (readFile == null)
            {
                return true;
            }

            _consoleOperator.WriteLine(readFile);
            return false;
        }

        public void Generate(string outputFile, int year, bool large, bool rtl, string inputFile, string holidayRepositoryPath)
        {
            var usage =
                $"Run {Path.GetFileName(_assemblyOperator.GetExecutingAssembly().CodeBase)} without any arguments to see usage help.";
            string font = null;
            var personalGregorianEvents = new PersonalGregorianEventList();
            if (inputFile != null && !ReadFile(inputFile, ref font, personalGregorianEvents))
            {
                _consoleOperator.WriteLine(usage);
                return;
            }

            var path = _pathOperator.GetDirectoryName(outputFile);
            if (path == string.Empty)
            {
                path = ".";
            }

            try
            {
                _directoryOperator.GetAccessControl(path);
            }
            catch (UnauthorizedAccessException)
            {
                _consoleOperator.WriteLine($"{path} is not writable.{Environment.NewLine}Generator aborted.");
                return;
            }
            catch (DirectoryNotFoundException)
            {
                _consoleOperator.WriteLine(
                    $"Directory {path} not found.{Environment.NewLine}Generator aborted.{Environment.NewLine}{usage}");
                return;
            }

            var application = _applicationFactory.Create();
            var windowState = application.WindowState;
            application.WindowState = XlWindowState.xlMinimized;
            var workbook = application.Workbooks.Add();
            try
            {
                workbook.SaveAs(outputFile);
            }
            catch (COMException)
            {
                FinalizeApplicationAndWriteLine(application, windowState, "Generator aborted.");
                return;
            }

            _consoleOperator.WriteLine($"{workbook.Name} is being created; please wait.");
            var excelCalendar = _excelCalendarFactory.Generate(large, rtl, font, workbook, personalGregorianEvents);
            foreach (var calendarEntry in
                _hebrewDateCalculator.CalendarYearStartingOn(new DateTime(year, 9, 1), holidayRepositoryPath))
            {
                excelCalendar.Add(calendarEntry);
            }

            excelCalendar.Save();
            FinalizeApplicationAndWriteLine(
                application, windowState, $"{workbook.Path}\\{workbook.Name} created successfully.");
        }

        private void FinalizeApplicationAndWriteLine(
            IApplicationWrapper application, XlWindowState windowState, string output)
        {
            application.WindowState = windowState;
            application.Quit();
            _consoleOperator.WriteLine(output);
        }
    }
}
