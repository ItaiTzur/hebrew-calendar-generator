﻿namespace ItaiTzur.utilities.hebrew_calendar_generator
{
    using ItaiTzur.utilities.hebrew_calendar_generator.Interfaces;
    using System.Reflection;

    internal sealed class AssemblyProvider : IAssemblyOperator
    {
        /// <summary>
        /// Gets the assembly that contains the code that is currently executing.
        /// </summary>
        /// <returns>The assembly that contains the code that is currently executing.</returns>
        public Assembly GetExecutingAssembly()
        {
            return Assembly.GetExecutingAssembly();
        }
    }
}
