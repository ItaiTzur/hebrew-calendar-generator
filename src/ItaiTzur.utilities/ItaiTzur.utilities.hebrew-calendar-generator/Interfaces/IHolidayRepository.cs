﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Interfaces
{
    using ItaiTzur.utilities.hebrew_calendar_generator.Entities.Contracts;
    using System.Collections.Generic;

    internal interface IHolidayRepository
    {
        IEnumerable<HolidayContract> GetAllHolidays();
    }
}
