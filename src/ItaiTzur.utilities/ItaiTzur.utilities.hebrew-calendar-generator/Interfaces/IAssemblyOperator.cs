﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Interfaces
{
    using System.Reflection;

    internal interface IAssemblyOperator
    {
        Assembly GetExecutingAssembly();
    }
}
