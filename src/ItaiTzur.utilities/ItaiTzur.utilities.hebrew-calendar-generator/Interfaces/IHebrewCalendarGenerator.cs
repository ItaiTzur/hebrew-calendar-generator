﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Interfaces
{
    internal interface IHebrewCalendarGenerator
    {
        void Generate(string outputFile, int year, bool large, bool rtl, string inputFile, string holidayRepositoryPath);
    }
}
