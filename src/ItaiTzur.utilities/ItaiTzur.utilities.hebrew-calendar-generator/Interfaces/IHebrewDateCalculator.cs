﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Interfaces
{
    using ItaiTzur.utilities.hebrew_calendar_generator.Entities;
    using System;
    using System.Collections.Generic;

    internal interface IHebrewDateCalculator
    {
        IEnumerable<CalendarEntry> CalendarYearStartingOn(DateTime firstDayInCalendar, string holidayRepositoryPath);

        string ReadFile(
            string[] allPersonalDateLines, ref string font, PersonalGregorianEventList personalGregorianEvents);
    }
}
