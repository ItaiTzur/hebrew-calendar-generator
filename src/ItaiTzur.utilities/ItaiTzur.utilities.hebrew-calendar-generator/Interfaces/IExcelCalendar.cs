﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Interfaces
{
    using ItaiTzur.utilities.hebrew_calendar_generator.Entities;

    internal interface IExcelCalendar
    {
        void Add(CalendarEntry calendarEntry);
        void Save();
    }
}
