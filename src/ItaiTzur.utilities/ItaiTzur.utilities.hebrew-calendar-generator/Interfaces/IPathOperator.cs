﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Interfaces
{
    internal interface IPathOperator
    {
        string GetDirectoryName(string path);
    }
}
