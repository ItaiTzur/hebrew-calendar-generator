﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Interfaces
{
    internal interface IConsoleOperator
    {
        void WriteLine(string value);
    }
}
