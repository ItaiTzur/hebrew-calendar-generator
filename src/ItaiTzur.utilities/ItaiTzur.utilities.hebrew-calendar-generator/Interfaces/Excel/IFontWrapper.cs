﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Interfaces.Excel
{
    internal interface IFontWrapper
    {
        dynamic Name { set; }
        dynamic Size { set; }
    }
}
