﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Interfaces.Excel
{
    internal interface IWorkbookWrapper
    {
        string Name { get; }
        string Path { get; }
        void Save();
        void SaveAs(object filename);
        ISheetsWrapper Worksheets { get; }
    }
}
