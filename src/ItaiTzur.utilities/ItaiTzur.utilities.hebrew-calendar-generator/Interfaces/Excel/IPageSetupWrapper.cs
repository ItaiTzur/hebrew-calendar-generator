﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Interfaces.Excel
{
    using Microsoft.Office.Interop.Excel;

    internal interface IPageSetupWrapper
    {
        double BottomMargin { set; }
        double LeftMargin { set; }
        XlPageOrientation Orientation { set; }
        double RightMargin { set; }
        double TopMargin { set; }
    }
}
