﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Interfaces.Excel
{
    using Microsoft.Office.Interop.Excel;

    internal interface IApplicationWrapper
    {
        void Quit();
        XlWindowState WindowState { get; set; }
        IWorkbooksWrapper Workbooks { get; }
    }
}
