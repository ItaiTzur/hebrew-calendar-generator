﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Interfaces.Excel
{
    internal interface IWorkbooksWrapper
    {
        IWorkbookWrapper Add();
    }
}
