﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Interfaces.Excel
{
    using Microsoft.Office.Interop.Excel;

    internal interface IRangeWrapper
    {
        IBorderWrapper Border(XlBordersIndex index);
        dynamic ColumnWidth { set; }
        IFontWrapper Font { get; }
        dynamic HorizontalAlignment { set; }
        IInteriorWrapper Interior { get; }
        int ReadingOrder { set; }
        dynamic RowHeight { set; }
        dynamic Value2 { set; }
        dynamic VerticalAlignment { set; }
        dynamic WrapText { set; }
    }
}
