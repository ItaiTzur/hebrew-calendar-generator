﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Interfaces.Excel
{
    interface IBorderWrapper
    {
        dynamic LineStyle { set; }
    }
}
