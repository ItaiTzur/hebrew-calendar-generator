﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Interfaces.Excel
{
    internal interface IWorksheetWrapper
    {
        void Activate();
        void CopyAfter(IWorksheetWrapper after);
        void Delete();
        bool DisplayRightToLeft { set; }
        string Name { set; }
        IPageSetupWrapper PageSetup { get; }
        IRangeWrapper Range(object cell1);
        IRangeWrapper Range(object cell1, object cell2);
    }
}
