﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Interfaces.Excel
{
    interface IApplicationFactory
    {
        IApplicationWrapper Create();
    }
}
