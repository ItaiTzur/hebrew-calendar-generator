﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Interfaces.Excel
{
    internal interface ISheetsWrapper
    {
        IWorksheetWrapper this[object index] { get; }
    }
}
