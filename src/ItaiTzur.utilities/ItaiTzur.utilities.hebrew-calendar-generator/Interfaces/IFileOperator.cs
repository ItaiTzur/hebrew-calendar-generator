﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Interfaces
{
    internal interface IFileOperator
    {
        /// <summary>
        /// Opens a text file, reads all lines of the file, and then closes the file.
        /// </summary>
        /// <param name="path">The file to open for reading.</param>
        /// <returns>A string array containing all lines of the file.</returns>
        string[] ReadAllLines(string path);
    }
}
