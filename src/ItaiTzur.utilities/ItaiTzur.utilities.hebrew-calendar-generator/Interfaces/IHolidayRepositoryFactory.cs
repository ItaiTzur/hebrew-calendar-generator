﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Interfaces
{
    internal interface IHolidayRepositoryFactory
    {
        IHolidayRepository Create(string path);
    }
}
