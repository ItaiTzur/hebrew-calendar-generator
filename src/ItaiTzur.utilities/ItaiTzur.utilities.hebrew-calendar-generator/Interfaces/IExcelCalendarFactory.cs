﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Interfaces
{
    using ItaiTzur.utilities.hebrew_calendar_generator.Entities;
    using ItaiTzur.utilities.hebrew_calendar_generator.Interfaces.Excel;

    internal interface IExcelCalendarFactory
    {
        IExcelCalendar Generate(
            bool isLarge,
            bool isRtl,
            string font,
            IWorkbookWrapper workbook,
            PersonalGregorianEventList personalGregorianEvents);
    }
}
