﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Interfaces
{
    using System.Security.AccessControl;

    internal interface IDirectoryOperator
    {
        DirectorySecurity GetAccessControl(string path);
    }
}
