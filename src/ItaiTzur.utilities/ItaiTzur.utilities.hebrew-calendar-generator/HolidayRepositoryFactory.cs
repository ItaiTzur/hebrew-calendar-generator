﻿namespace ItaiTzur.utilities.hebrew_calendar_generator
{
    using ItaiTzur.utilities.hebrew_calendar_generator.Interfaces;

    internal sealed class HolidayRepositoryFactory : IHolidayRepositoryFactory
    {
        public IHolidayRepository Create(string path)
        {
            return new HolidayRepository(path);
        }
    }
}
