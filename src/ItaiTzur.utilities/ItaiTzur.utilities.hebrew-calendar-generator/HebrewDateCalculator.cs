﻿namespace ItaiTzur.utilities.hebrew_calendar_generator
{
    using ItaiTzur.utilities.hebrew_calendar_generator.Entities;
    using ItaiTzur.utilities.hebrew_calendar_generator.Entities.Contracts.Converters;
    using ItaiTzur.utilities.hebrew_calendar_generator.Enums;
    using ItaiTzur.utilities.hebrew_calendar_generator.Extensions;
    using ItaiTzur.utilities.hebrew_calendar_generator.Interfaces;
    using System;
    using System.Linq;
    using System.Collections.Generic;

    internal sealed class HebrewDateCalculator : IHebrewDateCalculator
    {
        public const string NewLineWithoutCarriageReturn = "\n"; // used instead of Environment.NewLine which messes-up lines in Excel
        private const int GregBaseYear = 1760;
        private const int HebrBaseYear = 5530;
        private const int BaseMoladDay = 3560;
        private const int BaseMoladHour = 21;
        private const int BaseMoladPart = 549;

        private static readonly Dictionary<int, Tuple<HebrewYearAndMonth, int>> _absoluteDateToHebrewDateCache =
            new Dictionary<int, Tuple<HebrewYearAndMonth, int>>();

        private readonly IHolidayRepositoryFactory _holidayRepositoryFactory;
        private readonly Dictionary<HebrewMonthEnum, int, List<PersonalEvent>> _personalHebrewEvents =
            new Dictionary<HebrewMonthEnum, int, List<PersonalEvent>>();

        public HebrewDateCalculator(IHolidayRepositoryFactory holidayRepositoryFactory)
        {
            _holidayRepositoryFactory = holidayRepositoryFactory;
        }

        private static int StrToAbs(string str, bool includeThousands = false)
        {
            if (string.IsNullOrEmpty(str))
            {
                return 0;
            }

            int result;
            if (includeThousands)
            {
                result = StrToAbs(str.Substring(0, 1)) * 1000;
                str = str.Substring(1);
            }
            else
            {
                result = 0;
            }

            foreach (var character in str)
            {
                if (character >= 'א' && character <= 'י')
                {
                    result += character + 1 - 'א';
                }
                else if (character >= 'ק' && character <= 'ת')
                {
                    result += (character + 1 - 'ק') * 100;
                }
                else
                {
                    switch (character)
                    {
                        case 'ך':
                        case 'כ':
                            result += 20;
                            break;
                        case 'ל':
                            result += 30;
                            break;
                        case 'ם':
                        case 'מ':
                            result += 40;
                            break;
                        case 'ן':
                        case 'נ':
                            result += 50;
                            break;
                        case 'ס':
                            result += 60;
                            break;
                        case 'ע':
                            result += 70;
                            break;
                        case 'ף':
                        case 'פ':
                            result += 80;
                            break;
                        case 'ץ':
                        case 'צ':
                            result += 90;
                            break;
                    }
                }
            }

            return result;
        }

        public string ReadFile(
            string[] allPersonalDateLines, ref string font, PersonalGregorianEventList personalGregorianEvents)
        {
            font = allPersonalDateLines[0];
            for (var personalDateLineIndex1Based = 2;
                personalDateLineIndex1Based <= allPersonalDateLines.Length;
                ++personalDateLineIndex1Based)
            {
                var line = allPersonalDateLines[personalDateLineIndex1Based - 1];
                if (line.StartsWith("\t"))
                {
                    continue;
                }

                line = line.TrimStart();
                if (line == string.Empty)
                {
                    continue;
                }

//                _consoleOperator.WriteLine($"Reading line {i}...");
                var tabIndex = line.IndexOf('\t');
                var day = line.Substring(0, tabIndex);
                var error = int.TryParse(day, out var gregorianDay)
                    ? ReadGregorianDateEvent(gregorianDay, personalDateLineIndex1Based, line, tabIndex, personalGregorianEvents)
                    : ReadHebrewDateEvent(day, personalDateLineIndex1Based, line, tabIndex);
                if (error != null)
                {
                    return error;
                }
            }

            return null;
        }

        public string ReadGregorianDateEvent(
            int day,
            int personalDateLineIndex,
            string line,
            int space,
            PersonalGregorianEventList personalGregorianEvents)
        {
            if (day < 1 || day > 31)
            {
                return $"Invalid day {day} on line {personalDateLineIndex}"; // TODO: the proper way about this is an exception
            }

            line = line.Substring(space + 1).TrimStart();
            space = line.IndexOf('\t');
            var c = line.Substring(0, space);
            if (!int.TryParse(c, out var month))
            {
                return $"Non-numeric month {c} on line {personalDateLineIndex}";
            }

            if (month < 1 || month > 12)
            {
                return $"Invalid month {month} on line {personalDateLineIndex}";
            }

            if (day == 31 && (month == 4 || month == 6 || month == 9 || month == 11) ||
                day > 29 && month == 2)
            {
                return $"Invalid date {day}.{month} on line {personalDateLineIndex}";
            }

            return ReadYearAndAddPersonalEvent(
                line.Substring(space + 1).TrimStart(new[] { ' ' }),
                personalDateLineIndex,
                personalGregorianEvents,
                month,
                day);
        }

        public string ReadYearAndAddPersonalEvent(
            string line,
            int personalDateLineIndex,
            PersonalGregorianEventList personalGregorianEvents,
            int month,
            int day)
        {
            var tabIndex = line.IndexOf('\t');
            var yearString = line.Substring(0, tabIndex);
            int? year;
            if (yearString == string.Empty)
            {
                year = null;
            }
            else if (int.TryParse(yearString, out var intYear))
            {
                try
                {
                    new DateTime(intYear, month, day);
                }
                catch (ArgumentOutOfRangeException)
                {
                    return $"Invalid date {day}.{month}.{intYear} on line {personalDateLineIndex}";
                }

                year = intYear;
            }
            else
            {
                return $"Non-numeric year {yearString} on line {personalDateLineIndex}";
            }

            personalGregorianEvents.Add(month, day, year, line.Substring(tabIndex + 1).Trim());
            return null;
        }

        public string ReadHebrewDateEvent(string hebrewDay, int personalDateLineIndex, string line, int space)
        {
            var day = StrToAbs(hebrewDay);
            if (day < 1 || day > 30)
            {
                return $"Invalid day {day} on line {personalDateLineIndex}";
            }

            line = line.Substring(space + 1).TrimStart();
            space = line.IndexOf('\t');
            var hebrewMonth = line.Substring(0, space);
            var hebrewMonthEnumConverter = new HebrewMonthEnumConverter();
            HebrewMonthEnum month;
            try
            {
                month = hebrewMonthEnumConverter.NameToEnum(hebrewMonth);
            }
            catch (IndexOutOfRangeException)
            {
                return $"Invalid Hebrew month {hebrewMonth} on line {personalDateLineIndex}";
            }

            line = line.Substring(space + 1).TrimStart(new[] { ' ' });
            space = line.IndexOf('\t');
            var personalEventName = line.Substring(space + 1);
            line = line.Substring(0, space);
            _personalHebrewEvents.GetOrCreate(month, day).Add(
                new PersonalEvent(line != string.Empty ? StrToAbs(line, true) : (int?) null, personalEventName));
            return null;
        }

        public static readonly string[] HebrewMonthDayNames =
        {
            "א", "ב", "ג", "ד", "ה", "ו", "ז", "ח", "ט", "י", "יא",
            "יב", "יג", "יד", "טו", "טז", "יז", "יח", "יט", "כ", "כא", "כב", "כג", "כד", "כה", "כו", "כז", "כח", "כט",
            "ל"
        };

        private static bool HebrLeapYear(int year)
        {
            switch (year % 19)
            {
                case 0:
                case 3:
                case 6:
                case 8:
                case 11:
                case 14:
                case 17:
                    return true;
            }

            return false;
        }

        //NORMALIZE_MOLAD(d,h,p)   (h)+=(p)/1080;p%=1080;(d)+=(h)/24;(h)%=24;
        /* basically make parts < 1080 and hours < 24 */
        private static void NormalizeMolad(ref int days, ref int hours, ref int parts)
        {
            AddPartsToWhole(ref hours, ref parts, 1080);
            AddPartsToWhole(ref days, ref hours, 24);
        }

        private static void AddPartsToWhole(ref int whole, ref int parts, int numberOfPartsInAWhole)
        {
            whole += parts / numberOfPartsInAWhole;
            parts %= numberOfPartsInAWhole;
        }

//DAY_IN_WEEK(absolute_day)      (((absolute_day) + 2) % 7)
    /* This works because absolute_day 1 is a Friday */
        private static int DayInWeek(int absoluteDay)
        {
            return (absoluteDay + 2) % 7;
        }

        private static bool NoSoonerThan(int hour, int soonestHour, int part, int soonestPart)
        {
            return (hour > soonestHour) || (hour == soonestHour && part >= soonestPart);
        }

        private static int HebrewFindRosh(int hebrewYear) /* return absolute day of rosh of a year */
        {
            /* calculate how many moons since base time */
            var cycles = (hebrewYear - HebrBaseYear) / 19;
            var cycleYear = hebrewYear % 19;
            if (cycleYear == 0)
            {
                cycleYear = 19;
            }

//  for (loop = 1,extra_months = 0;loop < cycleYear;loop++)
//    extra_months += (HEBR_LEAP_YEAR(loop) ? 13 : 12);
            var extraMonths = Enumerable.Range(1, cycleYear - 1)
                .Aggregate(0, (current, loop) => current + (HebrLeapYear(loop) ? 13 : 12));
            /* add in the number of moons to base time */
            var moladDay = BaseMoladDay + 6939 * cycles + 29 * extraMonths;
            var moladHour = BaseMoladHour + 16 * cycles + 12 * extraMonths;
            var moladPart = BaseMoladPart + 595 * cycles + 793 * extraMonths;
            NormalizeMolad(ref moladDay, ref moladHour, ref moladPart);
            /* Now adjust Rosh date due to dehioth */
            var weekday = DayInWeek(moladDay);
            if (weekday == 0 || weekday == 3 || weekday == 5)
            {
                ++moladDay;
            }
            else if (moladHour >= 18)
            {
                if (weekday == 6 || weekday == 2 || weekday == 4)
                {
                    moladDay += 2;
                }
                else
                {
                    ++moladDay;
                }
            }
            else if (!HebrLeapYear(hebrewYear) && weekday == 2 &&
                NoSoonerThan(moladHour, 9, moladPart, 204))
            {
                moladDay += 2;
            }
            else if (HebrLeapYear(hebrewYear - 1) && !HebrLeapYear(hebrewYear) &&
                weekday == 1 &&
                NoSoonerThan(moladHour, 15, moladPart, 589))
            {
                ++moladDay;
            }

            return moladDay;
        }

        private static int HebrewYearLength(int hebrewYear)
        {
            return HebrewFindRosh(hebrewYear + 1) - HebrewFindRosh(hebrewYear);
        }

        internal static bool KislevHas29Days(int year)
        {
            var hebrewYearLength = HebrewYearLength(year);
            return hebrewYearLength == 353 || hebrewYearLength == 383;
        }

        private static int HebrewDaysInMonth(HebrewMonthEnum month, int year)
        {
            switch (month)
            {
                case HebrewMonthEnum.Tishrei:
                case HebrewMonthEnum.Shevat:
                case HebrewMonthEnum.Adar1:
                case HebrewMonthEnum.Nisan:
                case HebrewMonthEnum.Sivan:
                case HebrewMonthEnum.Av:
                    return 30;
                case HebrewMonthEnum.Teveth:
                case HebrewMonthEnum.Adar2:
                case HebrewMonthEnum.Iyar:
                case HebrewMonthEnum.Tammuz:
                case HebrewMonthEnum.Elul:
                    return 29;
                case HebrewMonthEnum.Heshvan:
                    var hebrewYearLength = HebrewYearLength(year);
                    return hebrewYearLength == 355 || hebrewYearLength == 385 ? 30 : 29;
                case HebrewMonthEnum.Kislev:
                    return KislevHas29Days(year) ? 29 : 30;
                case HebrewMonthEnum.Adar:
                    return HebrewYearLength(year) > 365 ? 30 : 29;
            }

            throw new IndexOutOfRangeException($"Undefined month '{month}'");
        }

        private static HebrewMonthEnum HebrewNextMonth(HebrewMonthEnum month, int year)
        {
            switch (month)
            {
                case HebrewMonthEnum.Tishrei:
                    return HebrewMonthEnum.Heshvan;
                case HebrewMonthEnum.Heshvan:
                    return HebrewMonthEnum.Kislev;
                case HebrewMonthEnum.Kislev:
                    return HebrewMonthEnum.Teveth;
                case HebrewMonthEnum.Teveth:
                    return HebrewMonthEnum.Shevat;
                case HebrewMonthEnum.Shevat:
                    return HebrLeapYear(year) ? HebrewMonthEnum.Adar1 : HebrewMonthEnum.Adar;
                case HebrewMonthEnum.Adar:
                case HebrewMonthEnum.Adar2:
                    return HebrewMonthEnum.Nisan;
                case HebrewMonthEnum.Adar1:
                    return HebrewMonthEnum.Adar2;
                case HebrewMonthEnum.Nisan:
                    return HebrewMonthEnum.Iyar;
                case HebrewMonthEnum.Iyar:
                    return HebrewMonthEnum.Sivan;
                case HebrewMonthEnum.Sivan:
                    return HebrewMonthEnum.Tammuz;
                case HebrewMonthEnum.Tammuz:
                    return HebrewMonthEnum.Av;
                case HebrewMonthEnum.Av:
                    return HebrewMonthEnum.Elul;
                case HebrewMonthEnum.Elul:
                    return HebrewMonthEnum.Tishrei;
            }

            throw new IndexOutOfRangeException($"Undefined month '{month}'");
        }

        private static void AddHolidayEve(List<Tuple<string, DaysOff>> holidays, string holiday, DaysOff daysOff)
        {
            AddHoliday(holidays, $"ערב {holiday}", daysOff == DaysOff.Full ? DaysOff.Half : DaysOff.None);
        }

        private static void AddHoliday(List<Tuple<string, DaysOff>> holidays, string holiday, DaysOff daysOff)
        {
            holidays.Add(new Tuple<string, DaysOff>(holiday, daysOff));
        }

        private static string HolidayNameOffset(GenericHoliday genericHoliday, int offset)
        {
            return $"{genericHoliday.Name} ({(offset > 0 ? "נדחה" : "הוקדם")})";
        }

        public IEnumerable<CalendarEntry> CalendarYearStartingOn(
            DateTime firstDayInCalendar, string holidayRepositoryPath)
        {
            var genericHolidays = LoadHolidays(holidayRepositoryPath);
            var currentGregorian = firstDayInCalendar;
            var currentAbsolute = AbsoluteFromGregorian(currentGregorian);
            var endingGregorian = firstDayInCalendar.AddYears(1);
            do
            {
                HebrewFromAbsolute(currentAbsolute, out var hebrewYearAndMonth, out var hebrewDay);
                var holidays = AddHolidaysIfNecessary(genericHolidays, currentAbsolute, currentGregorian);
                var holidayName = holidays.Any()
                    ? holidays.Aggregate(string.Empty, (current, nextHoliday) => $"{current} - {nextHoliday.Item1}").Substring(3)
                    : null;
                var daysOff = holidays.Any(toBeWritten => toBeWritten.Item2 == DaysOff.Full)
                    ? DaysOff.Full
                    : holidays.Any(toBeWritten => toBeWritten.Item2 == DaysOff.Half) ? DaysOff.Half : DaysOff.None;
                _personalHebrewEvents.TryGetValue(hebrewYearAndMonth.Month, hebrewDay, out var personalHebrewEvents);
                yield return new CalendarEntry(
                    currentGregorian, hebrewYearAndMonth, hebrewDay, personalHebrewEvents, holidayName, daysOff);
                currentGregorian = currentGregorian.AddDays(1);
                ++currentAbsolute;
            } while (currentGregorian < endingGregorian);
        }

        private GenericHoliday[] LoadHolidays(string holidayRepositoryPath)
        {
            var holidayConverter = new HolidayConverter();
            return _holidayRepositoryFactory.Create(holidayRepositoryPath).GetAllHolidays()
                .Select(holidayContract => holidayConverter.ToHolidayEntity(holidayContract)).ToArray();
        }

        private static int AbsoluteFromGregorian(DateTime date)
        {
            return (int)(date - new DateTime(GregBaseYear, 1, 1)).TotalDays;
        }

        private IReadOnlyList<Tuple<string, DaysOff>> AddHolidaysIfNecessary(
            IEnumerable<GenericHoliday> genericHolidays, int currentAbsolute, DateTime currentGregorian)
        {
            var holidays = new List<Tuple<string, DaysOff>>();
            foreach (var genericHoliday in genericHolidays)
            {
                var weekdayOffsets = genericHoliday.WeekdayOffsets;
                if (!CheckIfCurrentAbsoluteOccursDuringHolidayAndHandleIt(genericHoliday, currentAbsolute, currentGregorian, weekdayOffsets, holidays) &&
                    !CheckIfCurrentAbsoluteOccursOnHolidayEveAndAddIt(genericHoliday, currentAbsolute, currentGregorian, weekdayOffsets, holidays))
                {
                    AddOffsetHolidays(weekdayOffsets, currentGregorian, genericHoliday, currentAbsolute, holidays);
                }
            }

            return holidays;
        }

        private static bool CheckIfCurrentAbsoluteOccursDuringHolidayAndHandleIt(
            GenericHoliday genericHoliday,
            int currentAbsolute,
            DateTime currentGregorian,
            IEnumerable<WeekdayOffset> weekdayOffsets,
            List<Tuple<string, DaysOff>> holidays)
        {
            var dayDuringHolidayOfAbsoluteDate = DayDuringHolidayOfAbsoluteDateOr0(genericHoliday, currentAbsolute);
            if (dayDuringHolidayOfAbsoluteDate <= 0)
            {
                return false;
            }

            var currentDayOfWeek = currentGregorian.DayOfWeek;
            if (weekdayOffsets.All(weekdayOffset => weekdayOffset.Weekday != currentDayOfWeek))
            {
                var name = genericHoliday.Name;
                AddHoliday(
                    holidays,
                    genericHoliday.DurationInDays > 1 ? $"{HebrewMonthDayNames[dayDuringHolidayOfAbsoluteDate - 1]}' {name}" : name,
                    genericHoliday.DaysOff);
            }

            return true;
        }

        private static bool CheckIfCurrentAbsoluteOccursOnHolidayEveAndAddIt(
            GenericHoliday genericHoliday,
            int currentAbsolute,
            DateTime currentGregorian,
            IEnumerable<WeekdayOffset> weekdayOffsets,
            List<Tuple<string, DaysOff>> holidays)
        {
            if (genericHoliday.HasAHolidayEve && AbsoluteDateOccursDuringHoliday(genericHoliday, currentAbsolute + 1))
            {
                var nextDayOfWeek = currentGregorian.AddDays(1).DayOfWeek;
                if (weekdayOffsets.All(weekdayOffset => weekdayOffset.Weekday != nextDayOfWeek))
                {
                    AddHolidayEve(holidays, genericHoliday.Name, genericHoliday.DaysOff);
                    return true;
                }
            }

            return false;
        }

        private static bool AbsoluteDateOccursDuringHoliday(GenericHoliday genericHoliday, int absoluteDate)
        {
            return DayDuringHolidayOfAbsoluteDateOr0(genericHoliday, absoluteDate) > 0;
        }

        private void AddOffsetHolidays(
            IEnumerable<WeekdayOffset> weekdayOffsets,
            DateTime currentGregorian,
            GenericHoliday genericHoliday,
            int currentAbsolute,
            List<Tuple<string, DaysOff>> holidays)
        {
            foreach (var weekdayOffset in weekdayOffsets)
            {
                var offset = weekdayOffset.Offset;
                if (weekdayOffset.Weekday == (currentGregorian.AddDays(-offset)).DayOfWeek &&
                    AbsoluteDateOccursDuringHoliday(genericHoliday, currentAbsolute - offset))
                {
                    AddHoliday(holidays, HolidayNameOffset(genericHoliday, offset), genericHoliday.DaysOff);
                    break;
                }

                if (genericHoliday.HasAHolidayEve &&
                    weekdayOffset.Weekday == (currentGregorian.AddDays(1 - offset)).DayOfWeek &&
                    AbsoluteDateOccursDuringHoliday(genericHoliday, currentAbsolute + 1 - offset))
                {
                    AddHolidayEve(holidays, HolidayNameOffset(genericHoliday, offset), genericHoliday.DaysOff);
                    break;
                }
            }
        }

        private static int DayDuringHolidayOfAbsoluteDateOr0(GenericHoliday genericHoliday, int absoluteDate)
        {
            HebrewFromAbsolute(absoluteDate - genericHoliday.Day + 1, out var hebrewYearAndMonth, out var hebrewDay);
            return genericHoliday.Months.Contains(hebrewYearAndMonth.Month) && hebrewDay <= genericHoliday.DurationInDays ? hebrewDay : 0;
        }

        private static void HebrewFromAbsolute(
            int absoluteDay, out HebrewYearAndMonth hebrewYearAndMonth, out int hebrewDay)
        {
            if (_absoluteDateToHebrewDateCache.TryGetValue(absoluteDay, out var hebrewDate))
            {
                hebrewYearAndMonth = hebrewDate.Item1;
                hebrewDay = hebrewDate.Item2;
                return;
            }

  /* first find year - roughly under calculate year - take out chunks
     slighly bigger cycle or year to guarentee start at year at or
     before date */
            var nineteenYearCycles = 0;
            var days = absoluteDay - BaseMoladDay;
            AddPartsToWhole(ref nineteenYearCycles, ref days, 6950);
            /* have rough year, bounce it forward till you find the proper year */
/*  for (year = HEBR_BASE_YEAR + (19 * cycles) + delta - 1,
       year_date = hebrew_find_rosh(year),
       days = absolute_day;
      days > (next_year_date = hebrew_find_rosh(year + 1));
      year++,year_date = next_year_date)
    ;*/
            var year = HebrBaseYear + 19 * nineteenYearCycles + ChangeDaysToYears(days) - 1;
            var yearDate = HebrewFindRosh(year);
            year = CalculateYearAndRosh(year, ref yearDate, absoluteDay);
            days = absoluteDay - yearDate;
            hebrewYearAndMonth = new HebrewYearAndMonth(year, CalculateMonthAndRemainingDays(ref days, year));
            /* remainder is days */
            hebrewDay = days + 1;
            _absoluteDateToHebrewDateCache.Add(
                absoluteDay, new Tuple<HebrewYearAndMonth, int>(hebrewYearAndMonth, hebrewDay));
        }

        private static int ChangeDaysToYears(int days)
        {
            var years = 0;
            while (days >= 0)
            {
                ++years;
                days -= HebrLeapYear(years) ? 358 : 388;
            }

            return years;
        }

        private static int CalculateYearAndRosh(int year, ref int yearDate, int days)
        {
            do
            {
                var nextYearDate = HebrewFindRosh(year + 1);
                if (days < nextYearDate)
                {
                    return year;
                }

                ++year;
                yearDate = nextYearDate;
            } while (true);
        }

        private static HebrewMonthEnum CalculateMonthAndRemainingDays(ref int days, int year)
        {
            var month = HebrewMonthEnum.Tishrei;
            do
            {
                var hebrewDaysInMonth = HebrewDaysInMonth(month, year);
                if (days < hebrewDaysInMonth)
                {
                    return month;
                }

                days -= hebrewDaysInMonth;
                month = HebrewNextMonth(month, year);
            } while (true);
        }
    }
}
