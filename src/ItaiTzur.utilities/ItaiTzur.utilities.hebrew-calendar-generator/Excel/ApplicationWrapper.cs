﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Excel
{
    using ItaiTzur.utilities.hebrew_calendar_generator.Interfaces.Excel;
    using Microsoft.Office.Interop.Excel;

    internal sealed class ApplicationWrapper : IApplicationWrapper
    {
        private readonly Application _application = new Application { Visible = true };

        public void Quit()
        {
            _application.Quit();
        }

        public XlWindowState WindowState { get => _application.WindowState; set => _application.WindowState = value; }

        public IWorkbooksWrapper Workbooks => new WorkbooksWrapper(_application.Workbooks);
    }
}
