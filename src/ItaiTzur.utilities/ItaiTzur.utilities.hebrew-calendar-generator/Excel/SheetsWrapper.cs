﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Excel
{
    using ItaiTzur.utilities.hebrew_calendar_generator.Interfaces.Excel;
    using Microsoft.Office.Interop.Excel;

    internal sealed class SheetsWrapper : ISheetsWrapper
    {
        private readonly Sheets _sheets;

        internal SheetsWrapper(Sheets sheets)
        {
            _sheets = sheets;
        }

        public IWorksheetWrapper this[object index] => new WorksheetWrapper(_sheets[index]);
    }
}
