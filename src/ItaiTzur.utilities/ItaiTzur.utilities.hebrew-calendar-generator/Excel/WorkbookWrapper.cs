﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Excel
{
    using ItaiTzur.utilities.hebrew_calendar_generator.Interfaces.Excel;
    using Microsoft.Office.Interop.Excel;

    internal sealed class WorkbookWrapper : IWorkbookWrapper
    {
        private readonly Workbook _workbook;

        internal WorkbookWrapper(Workbook workbook)
        {
            _workbook = workbook;
        }

        public string Name => _workbook.Name;

        public string Path => _workbook.Path;

        public void Save()
        {
            _workbook.Save();
        }

        public void SaveAs(object filename)
        {
            _workbook.SaveAs(filename);
        }

        public ISheetsWrapper Worksheets => new SheetsWrapper(_workbook.Worksheets);
    }
}
