﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Excel
{
    using ItaiTzur.utilities.hebrew_calendar_generator.Interfaces.Excel;
    using Microsoft.Office.Interop.Excel;

    class BorderWrapper : IBorderWrapper
    {
        private readonly Border _border;

        internal BorderWrapper(Border border)
        {
            _border = border;
        }

        public dynamic LineStyle { set => _border.LineStyle = value; }
    }
}
