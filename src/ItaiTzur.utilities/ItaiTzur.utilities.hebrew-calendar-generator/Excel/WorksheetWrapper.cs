﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Excel
{
    using ItaiTzur.utilities.hebrew_calendar_generator.Interfaces.Excel;
    using Microsoft.Office.Interop.Excel;

    internal sealed class WorksheetWrapper : IWorksheetWrapper
    {
        private readonly _Worksheet _worksheet;

        internal WorksheetWrapper(_Worksheet worksheet)
        {
            _worksheet = worksheet;
        }

        public void Activate()
        {
            _worksheet.Activate();
        }

        public void CopyAfter(IWorksheetWrapper after)
        {
            _worksheet.Copy(After: ((WorksheetWrapper) after)._worksheet);
        }

        public void Delete()
        {
            _worksheet.Delete();
        }

        public bool DisplayRightToLeft { set => _worksheet.DisplayRightToLeft = value; }

        public string Name { set => _worksheet.Name = value; }

        public IPageSetupWrapper PageSetup => new PageSetupWrapper(_worksheet.PageSetup);

        public IRangeWrapper Range(object cell1)
        {
            return new RangeWrapper(_worksheet.Range[cell1]);
        }

        public IRangeWrapper Range(object cell1, object cell2)
        {
            return new RangeWrapper(_worksheet.Range[cell1, cell2]);
        }
    }
}
