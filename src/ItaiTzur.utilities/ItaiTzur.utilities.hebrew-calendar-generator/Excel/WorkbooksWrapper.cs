﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Excel
{
    using ItaiTzur.utilities.hebrew_calendar_generator.Interfaces.Excel;
    using Microsoft.Office.Interop.Excel;

    internal sealed class WorkbooksWrapper : IWorkbooksWrapper
    {
        private readonly Workbooks _workbooks;

        internal WorkbooksWrapper(Workbooks workbooks)
        {
            _workbooks = workbooks;
        }

        public IWorkbookWrapper Add()
        {
            return new WorkbookWrapper(_workbooks.Add());
        }
    }
}
