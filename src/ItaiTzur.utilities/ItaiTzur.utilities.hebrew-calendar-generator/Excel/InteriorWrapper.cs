﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Excel
{
    using ItaiTzur.utilities.hebrew_calendar_generator.Interfaces.Excel;
    using Microsoft.Office.Interop.Excel;

    internal sealed class InteriorWrapper : IInteriorWrapper
    {
        private readonly Interior _interior;

        internal InteriorWrapper(Interior interior)
        {
            _interior = interior;
        }

        public dynamic Color { set => _interior.Color = value; }
    }
}
