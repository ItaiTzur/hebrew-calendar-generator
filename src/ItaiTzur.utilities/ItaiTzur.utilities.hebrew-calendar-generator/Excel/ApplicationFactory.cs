﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Excel
{
    using ItaiTzur.utilities.hebrew_calendar_generator.Interfaces.Excel;

    internal sealed class ApplicationFactory : IApplicationFactory
    {
        public IApplicationWrapper Create()
        {
            return new ApplicationWrapper();
        }
    }
}
