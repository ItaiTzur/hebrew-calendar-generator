﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Excel
{
    using ItaiTzur.utilities.hebrew_calendar_generator.Interfaces.Excel;
    using Microsoft.Office.Interop.Excel;

    internal sealed class RangeWrapper : IRangeWrapper
    {
        private readonly Range _range;

        internal RangeWrapper(Range range)
        {
            _range = range;
        }

        public IBorderWrapper Border(XlBordersIndex index)
        {
            return new BorderWrapper(_range.Borders[index]);
        }

        public dynamic ColumnWidth { set => _range.ColumnWidth = value; }

        public IFontWrapper Font => new FontWrapper(_range.Font);

        public dynamic HorizontalAlignment { set => _range.HorizontalAlignment = value; }

        public IInteriorWrapper Interior => new InteriorWrapper(_range.Interior);

        public int ReadingOrder { set => _range.ReadingOrder = value; }

        public dynamic RowHeight { set => _range.RowHeight = value; }

        public dynamic Value2 { set => _range.Value2 = value; }

        public dynamic VerticalAlignment { set => _range.VerticalAlignment = value; }

        public dynamic WrapText { set => _range.WrapText = value; }
    }
}
