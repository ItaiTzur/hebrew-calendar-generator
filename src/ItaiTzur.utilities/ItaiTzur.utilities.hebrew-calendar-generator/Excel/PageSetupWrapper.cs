﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Excel
{
    using ItaiTzur.utilities.hebrew_calendar_generator.Interfaces.Excel;
    using Microsoft.Office.Interop.Excel;

    internal sealed class PageSetupWrapper : IPageSetupWrapper
    {
        private readonly PageSetup _pageSetup;

        internal PageSetupWrapper(PageSetup pageSetup)
        {
            _pageSetup = pageSetup;
        }

        public double BottomMargin { set => _pageSetup.BottomMargin = value; }

        public double LeftMargin { set => _pageSetup.LeftMargin = value; }

        public XlPageOrientation Orientation { set => _pageSetup.Orientation = value; }

        public double RightMargin { set => _pageSetup.RightMargin = value; }

        public double TopMargin { set => _pageSetup.TopMargin = value; }
    }
}
