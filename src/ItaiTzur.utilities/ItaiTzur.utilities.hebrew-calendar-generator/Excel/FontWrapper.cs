﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Excel
{
    using ItaiTzur.utilities.hebrew_calendar_generator.Interfaces.Excel;
    using Microsoft.Office.Interop.Excel;

    internal sealed class FontWrapper : IFontWrapper
    {
        private readonly Font _font;

        internal FontWrapper(Font font)
        {
            _font = font;
        }

        public dynamic Name { set => _font.Name = value; }

        public dynamic Size { set => _font.Size = value; }
    }
}
