﻿namespace ItaiTzur.utilities.hebrew_calendar_generator
{
    using ItaiTzur.utilities.hebrew_calendar_generator.Entities;
    using ItaiTzur.utilities.hebrew_calendar_generator.Interfaces;
    using ItaiTzur.utilities.hebrew_calendar_generator.Interfaces.Excel;

    internal sealed class ExcelCalendarFactory : IExcelCalendarFactory
    {
        public IExcelCalendar Generate(
            bool isLarge,
            bool isRtl,
            string font,
            IWorkbookWrapper workbook,
            PersonalGregorianEventList personalGregorianEvents)
        {
            return new ExcelCalendar(isLarge, isRtl, font, workbook, personalGregorianEvents);
        }
    }
}
