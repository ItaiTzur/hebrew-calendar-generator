﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Enums
{
    internal enum DaysOff
    { // replace this with bool? fullday - true means Full, false means Half, null means None
        None,
        Half,
        Full
    }
}
