﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Enums
{
    internal enum HebrewMonthEnum
    {
        Tishrei,
        Heshvan,
        Kislev,
        Teveth,
        Shevat,
        Adar,
        Adar1,
        Adar2,
        Nisan,
        Iyar,
        Sivan,
        Tammuz,
        Av,
        Elul
    }
}
