﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Extensions
{
    using System;

    internal static class ObjectExtensions
    {
        internal static TResult SafeFunc<TObject, TResult>(this TObject obj, Func<TObject, TResult> funcIfNotNull)
        {
            return obj == null ? default : funcIfNotNull(obj);
        }
    }
}
