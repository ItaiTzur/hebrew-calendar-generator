﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Extensions
{
    using ItaiTzur.utilities.hebrew_calendar_generator.Entities;
    using System;
    using System.Collections.Generic;

    internal static class DictionaryExtensions
    {
        internal static TValue GetOrCreate<TKey, TValue>(
            this IDictionary<TKey, TValue> dictionary, TKey key, Func<TValue> creator)
        {
            if (!dictionary.TryGetValue(key, out var result))
            {
                result = creator();
                dictionary[key] = result;
            }

            return result;
        }

        internal static TValue GetOrCreate<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key)
            where TValue : new()
        {
            return dictionary.GetOrCreate(key, () => new TValue());
        }

        internal static TValue GetOrCreate<TKey1, TKey2, TValue>(
            this Dictionary<TKey1, TKey2, TValue> dictionary, TKey1 key1, TKey2 key2, Func<TValue> creator)
        {
            if (!dictionary.TryGetValue(key1, key2, out var result))
            {
                result = creator();
                dictionary.Add(key1, key2, result);
            }

            return result;
        }

        internal static TValue GetOrCreate<TKey1, TKey2, TValue>(
            this Dictionary<TKey1, TKey2, TValue> dictionary, TKey1 key1, TKey2 key2) where TValue : new()
        {
            return dictionary.GetOrCreate(key1, key2, () => new TValue());
        }
    }
}
