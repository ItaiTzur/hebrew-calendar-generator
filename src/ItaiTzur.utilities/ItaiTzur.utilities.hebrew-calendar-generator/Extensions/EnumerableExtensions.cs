﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Extensions
{
    using System.Collections.Generic;

    internal static class EnumerableExtensions
    {
        internal static List<T> SafeCopy<T>(this IEnumerable<T> enumerable)
        {
            return enumerable == null ? new List<T>() : new List<T>(enumerable);
        }
    }
}
