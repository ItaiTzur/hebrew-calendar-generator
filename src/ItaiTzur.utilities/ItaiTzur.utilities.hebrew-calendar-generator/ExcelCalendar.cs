﻿namespace ItaiTzur.utilities.hebrew_calendar_generator
{
    using ItaiTzur.utilities.hebrew_calendar_generator.Entities;
    using ItaiTzur.utilities.hebrew_calendar_generator.Enums;
    using ItaiTzur.utilities.hebrew_calendar_generator.Extensions;
    using ItaiTzur.utilities.hebrew_calendar_generator.Interfaces;
    using ItaiTzur.utilities.hebrew_calendar_generator.Interfaces.Excel;
    using Microsoft.Office.Interop.Excel;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    internal sealed class ExcelCalendar : IExcelCalendar
    {
        private const int BottomMonthTitleRow = 27;
        private const char SundayColumn = 'B';
        private const char SaturdayColumn = (char)(SundayColumn + 6);
        private const int TopMonthTitleRow = 1;

        private readonly bool _isLarge;
        private readonly bool _isRtl;
        private readonly PersonalGregorianEventList _personalGregorianEvents;
        private readonly IWorkbookWrapper _workbook;

        private readonly IWorksheetWrapper _sheet1;
        private readonly string _gregorianMonthTitleCells;

        private int _currentGregorianMonth = 0;
        private bool _monthNeverChanged = true;
        private IWorksheetWrapper _currentSheet;
        private int _currentGregorianDayRow;
        private HebrewYearAndMonth _firstHebrewDateInCurrentGregorianMonth;
        private HebrewYearAndMonth _hebrewDateOfCurrentGregorianDate;

        public ExcelCalendar(
            bool isLarge,
            bool isRtl,
            string font,
            IWorkbookWrapper workbook,
            PersonalGregorianEventList personalGregorianEvents)
        {
            _workbook = workbook;
            _isLarge = isLarge;
            _isRtl = isRtl;
            _personalGregorianEvents = personalGregorianEvents;

            var sheets = _workbook.Worksheets;
            _sheet1 = sheets[1];
            _currentSheet = _sheet1;
            _gregorianMonthTitleCells = MonthTitleLocation(!isRtl, isLarge);
            DeleteRedundantSheets(sheets);
            PrepareSheet1(_sheet1, isRtl, isLarge, font, _gregorianMonthTitleCells);
        }

        private static string MonthTitleLocation(bool rightSide, bool large)
        {
            var column = rightSide ? 'B' : 'H';
            return $"{column}{TopMonthTitleRow}{(large ? $",{column}{BottomMonthTitleRow}" : null)}";
        }

        private static void DeleteRedundantSheets(ISheetsWrapper sheets)
        {
            sheets[3].Delete();
            sheets[2].Delete();
        }

        private void PrepareSheet1(
            IWorksheetWrapper sheet1, bool rtl, bool large, string font, string gregorianMonthTitleCells)
        {
            sheet1.DisplayRightToLeft = rtl;
            SetupPage(sheet1);
            SetMonthTitleFontSize(sheet1, large);
            SetupHebrewWeekdayCells(sheet1, large);
            SetupPersonalEventCells(sheet1, large, font);
            SetGregorianDayFontSize(sheet1, large);
            AlignGregorianMonthTitleCells(sheet1, gregorianMonthTitleCells);
            PrepareWeekdayTitles(sheet1);
            ShrinkColumn1Width(sheet1);
            ResizeWeekdayCells(sheet1, large);
            DrawVerticalBorders(sheet1, large);
            DrawHorizontalBorders(sheet1, large);
            DuplicateSheet1ForEntireYear(sheet1);
        }

        private static void SetupPage(IWorksheetWrapper worksheet)
        {
            var pageSetup = worksheet.PageSetup;
            pageSetup.TopMargin = 0;
            pageSetup.BottomMargin = 0;
            pageSetup.LeftMargin = 0;
            pageSetup.RightMargin = 0;
            pageSetup.Orientation = XlPageOrientation.xlLandscape;
        }

        private static void SetMonthTitleFontSize(IWorksheetWrapper worksheet, bool large)
        {
            worksheet.Range($"{TopMonthTitleRow}:{TopMonthTitleRow}{(large ? $",{BottomMonthTitleRow}:{BottomMonthTitleRow}" : null)}")
                .Font.Size = 20;
        }

        private static void SetupHebrewWeekdayCells(IWorksheetWrapper worksheet, bool large)
        {
            var range = worksheet.Range(large ? "3:4,7:8,11:12,15:16,19:20,23:24" : "3:3,5:5,7:7,9:9,11:11,13:13");
            range.Font.Size = 10;
            range.VerticalAlignment = XlVAlign.xlVAlignTop;
        }

        private static void SetupPersonalEventCells(IWorksheetWrapper worksheet, bool large, string font)
        {
            if (large)
            {
                var range = worksheet.Range("4:4,8:8,12:12,16:16,20:20,24:24");
                range.Font.Name = font;
                range.WrapText = true;
            }
        }

        private static void SetGregorianDayFontSize(IWorksheetWrapper worksheet, bool large)
        {
            var spacing = large ? 4 : 2;
            worksheet.Range(Enumerable.Range(1, 6).Select(item => item * spacing + 2)
                .Aggregate(string.Empty, (current, row) => $"{current},{row}:{row}").Substring(1)).Font.Size = 24;
        }

        private static void AlignGregorianMonthTitleCells(IWorksheetWrapper worksheet, string gregorianMonthTitleCells)
        {
            var range = worksheet.Range(gregorianMonthTitleCells);
            range.HorizontalAlignment = XlHAlign.xlHAlignLeft;
            range.ReadingOrder = (int)Constants.xlRTL;
        }

        private static void PrepareWeekdayTitles(IWorksheetWrapper worksheet)
        {
            var weekdayTitleRow = 2;
            foreach (var (name, index) in new[] { "ראשון", "שני", "שלישי", "רביעי", "חמישי", "שישי", "שבת" }.Select((name, index) => (name, index)))
            {
                worksheet.Range($"{(char)(SundayColumn + index)}{weekdayTitleRow}").Value2 = name;
            }

            var weekdayTitleRowRange = worksheet.Range($"{weekdayTitleRow}:{weekdayTitleRow}");
            weekdayTitleRowRange.Font.Size = 10;
            weekdayTitleRowRange.HorizontalAlignment = XlHAlign.xlHAlignCenter;
            worksheet.Range($"{SundayColumn}{weekdayTitleRow}", $"{SaturdayColumn}{weekdayTitleRow}")
                .Interior.Color = XlRgbColor.rgbGray;
        }

        private static void ShrinkColumn1Width(IWorksheetWrapper worksheet)
        {
            worksheet.Range("A1").ColumnWidth = .08;
        }

        private static void ResizeWeekdayCells(IWorksheetWrapper worksheet, bool large)
        {
            var range = worksheet.Range($"{SundayColumn}3", $"H{(large ? "26" : "14")}");
            range.ColumnWidth = /*19.71*/18.43;
            range.RowHeight = /*large ? 91.5 : */45.75;
        }

        private static void DrawVerticalBorders(IWorksheetWrapper worksheet, bool large)
        {
            worksheet.Range(Enumerable.Range('A', 'I' - 'A').Select(column => (char)column)
                .Aggregate(string.Empty, (current, column) => $"{current},{column}2:{column}{(large ? 26 : 14)}")
                .Substring(1)).Border(XlBordersIndex.xlEdgeRight).LineStyle = XlLineStyle.xlContinuous;
        }

        private static void DrawHorizontalBorders(IWorksheetWrapper worksheet, bool large)
        {
            var spacing = large ? 4 : 2;
            worksheet.Range(Enumerable.Range(0, 7).Select(item => item * spacing + 2).Aggregate($"{SundayColumn}1:{SaturdayColumn}1", (current, row) => $"{current},{SundayColumn}{row}:{SaturdayColumn}{row}"))
                .Border(XlBordersIndex.xlEdgeBottom).LineStyle = XlLineStyle.xlContinuous;
        }

        private static void DuplicateSheet1ForEntireYear(IWorksheetWrapper sheet1)
        {
            foreach (var _ in Enumerable.Range(0, 11))
            {
                sheet1.CopyAfter(sheet1);
            }
        }

        public void Add(CalendarEntry calendarEntry)
        {
            var hebrewYearAndMonth = calendarEntry.HebrewYearAndMonth;
            var gregorianDate = calendarEntry.GregorianDate;
            AdvanceGregorianMonthIfNecessary(gregorianDate, hebrewYearAndMonth);
            var currentDayOfWeek = gregorianDate.DayOfWeek;
            var currentDayOfWeekColumn = $"{(char)(SundayColumn + (int)currentDayOfWeek)}";
            var currentGregorianDayCell = currentDayOfWeekColumn + _currentGregorianDayRow;
            WriteGregorianDay(currentGregorianDayCell, gregorianDate);
            WritePersonalEvents(
                gregorianDate,
                calendarEntry.PersonalEvents,
                hebrewYearAndMonth.Year,
                currentDayOfWeekColumn);
            WriteHebrewDateAndHoliday(
                calendarEntry,
                currentDayOfWeekColumn,
                hebrewYearAndMonth.Month,
                currentDayOfWeek,
                currentGregorianDayCell);
            AdvanceCurrentGregorianDayRowIfNecessary(currentDayOfWeek);
        }

        public void AdvanceGregorianMonthIfNecessary(DateTime currentGregorian, HebrewYearAndMonth hebrewYearAndMonth)
        {
            if (_currentGregorianMonth != currentGregorian.Month)
            {
                if (_monthNeverChanged)
                {
                    _monthNeverChanged = false;
                }
                else
                {
                    WriteHebrewMonthTitle();
                }

                _firstHebrewDateInCurrentGregorianMonth = hebrewYearAndMonth;
                _currentGregorianMonth = currentGregorian.Month;
                _currentSheet = _workbook.Worksheets[(_currentGregorianMonth + 3) % 12 + 1];
                _currentGregorianDayRow = 2 + NumberOfRowsPerWeek();
                var gregorianMonthAndYearSuffix =
                    $" {currentGregorian.ToString("MMMM", CultureInfo.InvariantCulture)} {currentGregorian.Year}";
                _currentSheet.Name = $"{_currentGregorianMonth}{gregorianMonthAndYearSuffix}";
                string[] GregorianMonthNamesInHebrew =
                {
                    "יָנוּאָר", "פֶבְּרוּאַר", "מַרס", "אַפְּרִיל", "מַאי", "יוּנִי",
                    "יוּלִי", "אוֹגוּסט", "סֶפְּטֶמְבֶּר", "אוֹקְטוֹבֶּר", "נוֹבֶמְבֶּר", "דֶצֶמְּבֶּר"
                };
                _currentSheet.Range(_gregorianMonthTitleCells).Value2 =
                    $"{_currentGregorianMonth} {GregorianMonthNamesInHebrew[_currentGregorianMonth - 1]}{gregorianMonthAndYearSuffix}";
            }
            else
            {
                _hebrewDateOfCurrentGregorianDate = hebrewYearAndMonth;
            }
        }

        private int NumberOfRowsPerWeek()
        {
            return _isLarge ? 4 : 2;
        }

        private void WriteGregorianDay(string gregorianDayCell, DateTime gregorianDate)
        {
            _currentSheet.Range(gregorianDayCell).Value2 = gregorianDate.Day;
        }

        private void WritePersonalEvents(
            DateTime gregorianDate,
            List<PersonalEvent> personalHebrewEvents,
            int hebrewYear,
            string currentDayOfWeekColumn)
        {
            if (!_isLarge)
            {
                return;
            }

            var personalEventString = _personalGregorianEvents.TryGetValue(gregorianDate.Month, gregorianDate.Day, out var personalEvents)
                ? personalEvents.Aggregate(string.Empty, (current, personalGregorianEvent) => $"{current}\n{personalGregorianEvent.Name}{personalGregorianEvent.Year.SafeFunc(eventBaseYear => $" ({(gregorianDate.Year - eventBaseYear)})")}")
                : null;
            foreach (var personalHebrewEvent in personalHebrewEvents)
            {
                personalEventString +=
                    $"\n{personalHebrewEvent.Name}{personalHebrewEvent.Year.SafeFunc(eventBaseYear => $" ({hebrewYear - eventBaseYear})")}";
            }

            if (personalEventString != null)
            {
                _currentSheet.Range($"{currentDayOfWeekColumn}{_currentGregorianDayRow - 2}").Value2 =
                    personalEventString.Substring(1);
            }
        }

        private void WriteHebrewDateAndHoliday(
            CalendarEntry calendarEntry,
            string currentDayOfWeekColumn,
            HebrewMonthEnum hebrewMonth,
            DayOfWeek currentDayOfWeek,
            string currentGregorianDayCell)
        {
            var currentHebrewDayCell = currentDayOfWeekColumn + (_currentGregorianDayRow - NumberOfRowsPerWeek() + 1);
            WriteHebrewDate(currentHebrewDayCell, calendarEntry, hebrewMonth);
            GrayHolidayOut(
                currentDayOfWeek == DayOfWeek.Saturday ? DaysOff.Full : calendarEntry.DaysOff,
                currentDayOfWeekColumn,
                currentGregorianDayCell,
                currentHebrewDayCell);
        }

        private void WriteHebrewDate(
            string currentHebrewDayCell, CalendarEntry calendarEntry, HebrewMonthEnum hebrewMonth)
        {
            var range = _currentSheet.Range(currentHebrewDayCell);
            var holidayName = calendarEntry.HolidayName;
            var hebrewDateString =
                $"{HebrewDateCalculator.HebrewMonthDayNames[calendarEntry.HebrewDay - 1]} {NameOf(hebrewMonth)}";
            range.Value2 = holidayName == null
                ? hebrewDateString
                : $"{hebrewDateString}{HebrewDateCalculator.NewLineWithoutCarriageReturn}{holidayName}";
        }

        private static string NameOf(HebrewMonthEnum hebrewMonth)
        {
            string[] HebrewMonthNames =
            {
                "תִּשְׁרֵי", "חֶשְׁוָן", "כִּסְלֵו", "טֵבֵת", "שְׁבָט",
                "אֲדָר", "אֲדָר א", "אֲדָר ב", "נִיסָן", "אִיָּר", "סִיוָן", "תַּמּוּז", "אָב", "אֱלוּל"
            };
            return HebrewMonthNames[(int)hebrewMonth];
        }

        private void GrayHolidayOut(
            DaysOff daysOff, string currentDayOfWeekColumn, string currentGregorianDayCell, string currentHebrewDayCell)
        {
            IRangeWrapper range;
            switch (daysOff)
            {
                case DaysOff.Half:
                    range = _isLarge
                        ? _currentSheet.Range(currentDayOfWeekColumn + (_currentGregorianDayRow - 1), currentGregorianDayCell)
                        : _currentSheet.Range(currentGregorianDayCell);
                    break;
                case DaysOff.Full:
                    range = _currentSheet.Range(currentHebrewDayCell, currentGregorianDayCell);
                    break;
                default:
                    return;
            }

            range.Interior.Color = XlRgbColor.rgbLightGray;
        }

        private void AdvanceCurrentGregorianDayRowIfNecessary(DayOfWeek currentDayOfWeek)
        {
            if (currentDayOfWeek == DayOfWeek.Saturday)
            {
                _currentGregorianDayRow += NumberOfRowsPerWeek();
            }
        }

        public void Save()
        {
            WriteHebrewMonthTitle();
            _sheet1.Activate();
            _workbook.Save();
        }

        private void WriteHebrewMonthTitle()
        {
            var previousMonth = _firstHebrewDateInCurrentGregorianMonth.Month;
            var currentMonth = _hebrewDateOfCurrentGregorianDate.Month;
            var currentYear = _hebrewDateOfCurrentGregorianDate.Year;
            dynamic prefix;
            if (previousMonth == currentMonth)
            {
                prefix = null;
            }
            else
            {
                var previousYear = _firstHebrewDateInCurrentGregorianMonth.Year;
                prefix =
                    $"{NameOf(previousMonth)}{(previousYear == currentYear ? null : $" {AbsToStr(previousYear)}")} - ";
            }

            _currentSheet.Range(MonthTitleLocation(_isRtl, _isLarge)).Value2 =
                $"{prefix}{NameOf(currentMonth)} {AbsToStr(currentYear)}";
        }

        private static string AbsToStr(int hebYear, bool includeThousands = false)
        {
            var result = string.Empty;
            if (hebYear >= 1000)
            {
                if (includeThousands)
                {
                    result = YearToChar(hebYear / 1000);
                }

                hebYear %= 1000;
            }

            while (hebYear >= 400)
            {
                ProcChar2(ref hebYear, ref result, 400, 'ת');
            }

            if (!ProcChar(ref hebYear, ref result, 300, 'ש') && !ProcChar(ref hebYear, ref result, 200, 'ר'))
            {
                ProcChar(ref hebYear, ref result, 100, 'ק');
            }

            if (!ProcChar(ref hebYear, ref result, 90, 'צ') && !ProcChar(ref hebYear, ref result, 80, 'פ') &&
                !ProcChar(ref hebYear, ref result, 70, 'ע') && !ProcChar(ref hebYear, ref result, 60, 'ס') &&
                !ProcChar(ref hebYear, ref result, 50, 'נ') && !ProcChar(ref hebYear, ref result, 40, 'מ') &&
                !ProcChar(ref hebYear, ref result, 30, 'ל') && !ProcChar(ref hebYear, ref result, 20, 'כ'))
            {
                ProcChar(ref hebYear, ref result, 10, 'י');
            }

            if (hebYear > 0)
            {
                result += YearToChar(hebYear);
            }

            var l = result.Length - 1;
            return l > 0 ? result.Substring(0, l) + '"' + result.Substring(l) : result;
        }

        private static void ProcChar2(ref int hebYear, ref string result, int test, char chr)
        {
            result += chr;
            hebYear -= test;
        }

        private static bool ProcChar(ref int hebYear, ref string result, int test, char chr)
        {
            if (hebYear < test)
            {
                return false;
            }

            ProcChar2(ref hebYear, ref result, test, chr);
            return true;
        }

        private static string YearToChar(int digit)
        {
            return $"{(char)('א' + digit - 1)}";
        }
    }
}
