﻿namespace ItaiTzur.utilities.hebrew_calendar_generator
{
    using ItaiTzur.utilities.hebrew_calendar_generator.Interfaces;
    using System;

    internal sealed class ConsoleWrapper : IConsoleOperator
    {
        /// <summary>
        /// Writes the specified string value, followed by the current line terminator, to the standard output stream.
        /// </summary>
        /// <param name="value">The value to write.</param>
        public void WriteLine(string value)
        {
            Console.WriteLine(value);
        }
    }
}
