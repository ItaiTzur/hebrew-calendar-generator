﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Entities
{
    using ItaiTzur.utilities.hebrew_calendar_generator.Enums;
    using ItaiTzur.utilities.hebrew_calendar_generator.Extensions;
    using System.Collections.Generic;

    internal sealed class GenericHoliday
    {
        internal readonly string Name;
        internal readonly IEnumerable<HebrewMonthEnum> Months;
        internal readonly int Day;
        internal readonly int DurationInDays;
        internal readonly DaysOff DaysOff;
        internal readonly bool HasAHolidayEve;
        internal readonly IEnumerable<WeekdayOffset> WeekdayOffsets;

        internal GenericHoliday(
            string name,
            IEnumerable<HebrewMonthEnum> months,
            int day,
            int durationInDays,
            DaysOff daysOff,
            bool hasAHolidayEve,
            IEnumerable<WeekdayOffset> weekdayOffsets)
        { // this class represents a holiday in its most generic form, i.e. before calculating offsets for a specific year; as such, it can be saved into (and loaded from) a database or configuration file
            Name = name;
            Months = new List<HebrewMonthEnum>(months);
            Day = day;
            DurationInDays = durationInDays;
            DaysOff = daysOff;
            HasAHolidayEve = hasAHolidayEve;
            WeekdayOffsets = weekdayOffsets.SafeCopy();
        }
    }
}
