﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Entities
{
    using ItaiTzur.utilities.hebrew_calendar_generator.Enums;
    using ItaiTzur.utilities.hebrew_calendar_generator.Extensions;
    using System;
    using System.Collections.Generic;

    internal sealed class CalendarEntry
    {
        public readonly DateTime GregorianDate;
        public readonly HebrewYearAndMonth HebrewYearAndMonth;
        public readonly int HebrewDay;
        public readonly List<PersonalEvent> PersonalEvents;
        public readonly string HolidayName;
        public readonly DaysOff DaysOff;

        internal CalendarEntry(
            DateTime gregorianDate,
            HebrewYearAndMonth hebrewYearAndMonth,
            int hebrewDay,
            IEnumerable<PersonalEvent> personalEvents,
            string holidayName,
            DaysOff daysOff)
        {
            GregorianDate = gregorianDate;
            HebrewYearAndMonth = hebrewYearAndMonth;
            HebrewDay = hebrewDay;
            PersonalEvents = personalEvents.SafeCopy();
            HolidayName = holidayName;
            DaysOff = daysOff;
        }
    }
}
