﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Entities
{
    using ItaiTzur.utilities.hebrew_calendar_generator.Extensions;
    using System.Collections.Generic;

    internal sealed class PersonalGregorianEventList
     {
        private readonly Dictionary<int, int, List<PersonalEvent>> _monthAndDayMappingToPersonalEvents =
            new Dictionary<int, int, List<PersonalEvent>>();

        public void Add(int month, int day, int? year, string eventString)
        {
            _monthAndDayMappingToPersonalEvents.GetOrCreate(month, day).Add(new PersonalEvent(year, eventString));
        }

        public bool TryGetValue(int month, int day, out List<PersonalEvent> personalEvents)
        {
            return _monthAndDayMappingToPersonalEvents.TryGetValue(month, day, out personalEvents);
        }
    }
}
