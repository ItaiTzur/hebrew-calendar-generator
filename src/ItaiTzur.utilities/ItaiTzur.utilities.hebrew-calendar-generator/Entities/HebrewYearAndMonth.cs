﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Entities
{
    using ItaiTzur.utilities.hebrew_calendar_generator.Enums;

    internal sealed class HebrewYearAndMonth
    {
        internal readonly int Year;
        internal readonly HebrewMonthEnum Month;

        internal HebrewYearAndMonth(int year, HebrewMonthEnum month)
        {
            Year = year;
            Month = month;
        }
    }
}
