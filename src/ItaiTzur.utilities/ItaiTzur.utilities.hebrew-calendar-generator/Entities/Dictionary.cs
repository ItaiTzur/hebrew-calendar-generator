﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Entities
{
    using ItaiTzur.utilities.hebrew_calendar_generator.Extensions;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Represents a collection of double-keys and values.To browse the .NET Framework source code for the types
    /// underlying this type, see the Reference Source.
    /// </summary>
    /// <typeparam name="TKey1">The type of the first component of the keys in the dictionary.</typeparam>
    /// <typeparam name="TKey2">The type of the second component of the keys in the dictionary.</typeparam>
    /// <typeparam name="TValue">The type of the values in the dictionary.</typeparam>
    internal sealed class Dictionary<TKey1, TKey2, TValue>
    {
        private readonly Dictionary<TKey1, Dictionary<TKey2, TValue>> _dictionary =
            new Dictionary<TKey1, Dictionary<TKey2, TValue>>();

        /// <summary>
        /// Adds the specified keys and value to the dictionary.
        /// </summary>
        /// <param name="key1">The first component in the key of the element to add.</param>
        /// <param name="key2">The second component in the key of the element to add.</param>
        /// <param name="value">The value of the element to add. The value can be null for reference types.</param>
        /// <exception cref="ArgumentNullException" />
        /// <exception cref="ArgumentException" />
        internal void Add(TKey1 key1, TKey2 key2, TValue value)
        {
            _dictionary.GetOrCreate(key1).Add(key2, value);
        }

        /// <summary>
        /// Gets the value associated with the specified keys.
        /// </summary>
        /// <param name="key1">The first component in the key of the value to get.</param>
        /// <param name="key2">The second component in the key of the value to get.</param>
        /// <param name="value">
        /// When this method returns, contains the value associated with the specified keys, if the keys are found;
        /// otherwise, the default value for the type of the value parameter. This parameter is passed uninitialized.
        /// </param>
        /// <exception cref="ArgumentNullException" />
        internal bool TryGetValue(TKey1 key1, TKey2 key2, out TValue value)
        {
            if (!_dictionary.TryGetValue(key1, out var dictionary))
            {
                value = default;
                return false;
            }

            return dictionary.TryGetValue(key2, out value);
        }
    }
}
