﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Entities.Contracts.Converters
{
    using ItaiTzur.utilities.hebrew_calendar_generator.Enums;
    using System;

    internal sealed class DaysOffConverter
    {
        internal DaysOff NameToDaysOff(string daysOff)
        {
            switch (daysOff)
            {
                case "None":
                    return DaysOff.None;
                case "Half":
                    return DaysOff.Half;
                case "Full":
                    return DaysOff.Full;
            }

            throw new IndexOutOfRangeException($"Undefined days-off identifier '{daysOff}'");
        }
    }
}
