﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Entities.Contracts.Converters
{
    using ItaiTzur.utilities.hebrew_calendar_generator.Extensions;
    using System.Linq;

    internal sealed class HolidayConverter
    {
        internal GenericHoliday ToHolidayEntity(HolidayContract holidayContract)
        {
            return new GenericHoliday(
                holidayContract.Name.Aggregate(string.Empty, (current, name) => $"{current}{HebrewDateCalculator.NewLineWithoutCarriageReturn}{name}").Substring(HebrewDateCalculator.NewLineWithoutCarriageReturn.Length),
                holidayContract.GetMonths(),
                holidayContract.Day,
                holidayContract.DurationInDays,
                holidayContract.GetDaysOff(),
                holidayContract.HasAHolidayEve,
                holidayContract.WeekdayOffsets.SafeFunc(weekdayOffsetContracts => weekdayOffsetContracts.Select(weekdayOffsetContract => ToWeekdayOffsetEntity(weekdayOffsetContract))));
        }

        internal WeekdayOffset ToWeekdayOffsetEntity(WeekdayOffsetContract offset)
        {
            return new WeekdayOffset(offset.Weekday, offset.Offset);
        }
    }
}
