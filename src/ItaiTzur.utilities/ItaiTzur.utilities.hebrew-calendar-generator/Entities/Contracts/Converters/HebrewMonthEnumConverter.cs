﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Entities.Contracts.Converters
{
    using ItaiTzur.utilities.hebrew_calendar_generator.Enums;
    using System;

    internal sealed class HebrewMonthEnumConverter
    {
        internal HebrewMonthEnum NameToEnum(string month)
        {
            switch (month)
            {
                case "תשרי":
                    return HebrewMonthEnum.Tishrei;
                case "חשון":
                    return HebrewMonthEnum.Heshvan;
                case "כסלו":
                    return HebrewMonthEnum.Kislev;
                case "טבת":
                    return HebrewMonthEnum.Teveth;
                case "שבט":
                    return HebrewMonthEnum.Shevat;
                case "אדר":
                    return HebrewMonthEnum.Adar;
                case "אדר א":
                    return HebrewMonthEnum.Adar1;
                case "אדר ב":
                    return HebrewMonthEnum.Adar2;
                case "ניסן":
                    return HebrewMonthEnum.Nisan;
                case "איר":
                    return HebrewMonthEnum.Iyar;
                case "סיון":
                    return HebrewMonthEnum.Sivan;
                case "תמוז":
                    return HebrewMonthEnum.Tammuz;
                case "אב":
                    return HebrewMonthEnum.Av;
                case "אלול":
                    return HebrewMonthEnum.Elul;
            }

            throw new IndexOutOfRangeException($"Undefined month '{month}'");
        }
    }
}
