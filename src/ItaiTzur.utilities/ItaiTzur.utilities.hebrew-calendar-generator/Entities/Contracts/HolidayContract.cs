﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Entities.Contracts
{
    using ItaiTzur.utilities.hebrew_calendar_generator.Entities.Contracts.Converters;
    using ItaiTzur.utilities.hebrew_calendar_generator.Enums;
    using System;
    using System.Linq;

    internal sealed class HolidayContract
    {
        public string[] Name { get; set; }

        private HebrewMonthEnum[] _months;
        public string[] Months
        {
            get => _months.Select(hebrewMonthEnum => hebrewMonthEnum.ToString()).ToArray();
            set
            {
                var hebrewMonthEnumConverter = new HebrewMonthEnumConverter();
                _months = value.Select(hebrewMonthString => hebrewMonthEnumConverter.NameToEnum(hebrewMonthString))
                    .ToArray();
            }
        }

        internal System.Collections.Generic.IEnumerable<HebrewMonthEnum> GetMonths()
        {
            return _months;
        }

        public int Day { get; set; }

        private int _durationInDays = 1;
        public int DurationInDays { get => _durationInDays; set => _durationInDays = value; }

        private DaysOff _daysOff;
        public string DaysOff
        {
            get => _daysOff.ToString();
            set
            {
                var daysOffConverter = new DaysOffConverter();
                _daysOff = daysOffConverter.NameToDaysOff(value);
            }
        }

        internal DaysOff GetDaysOff()
        {
            return _daysOff;
        }

        public bool HasAHolidayEve { get; set; }

        public WeekdayOffsetContract[] WeekdayOffsets { get; set; }
    }
}
