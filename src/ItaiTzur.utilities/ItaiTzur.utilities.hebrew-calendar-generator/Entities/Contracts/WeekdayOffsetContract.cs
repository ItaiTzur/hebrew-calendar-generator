﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Entities.Contracts
{
    using System;

    internal sealed class WeekdayOffsetContract
    {
        public DayOfWeek Weekday { get; set; }

        public int Offset { get; set; }
    }
}
