﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Entities
{
    internal sealed class PersonalEvent
    {
        internal readonly int? Year;
        internal readonly string Name;

        internal PersonalEvent(int? year, string name)
        {
            Year = year;
            Name = name;
        }
    }
}
