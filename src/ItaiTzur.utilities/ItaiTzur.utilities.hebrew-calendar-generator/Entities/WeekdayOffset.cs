﻿namespace ItaiTzur.utilities.hebrew_calendar_generator.Entities
{
    using System;

    internal sealed class WeekdayOffset
    {
        internal readonly DayOfWeek Weekday;
        internal readonly int Offset;

        internal WeekdayOffset(DayOfWeek weekday, int offset)
        {
            Weekday = weekday;
            Offset = offset;
        }
    }
}
