﻿namespace ItaiTzur.utilities.hebrew_calendar_generator
{
    using ItaiTzur.utilities.hebrew_calendar_generator.Interfaces;
    using System.IO;

    internal sealed class PathWrapper : IPathOperator
    {
        /// <summary>
        /// Returns the directory information for the specified path string.
        /// </summary>
        /// <param name="path">The path of a file or directory.</param>
        /// <returns>
        /// Directory information for <paramref name="path"/>, or null if <paramref name="path"/> denotes a root
        /// directory or is null. Returns <see cref="string"/>.Empty if <paramref name="path"/> does not contain
        /// directory information.
        /// </returns>
        public string GetDirectoryName(string path)
        {
            return Path.GetDirectoryName(path);
        }
    }
}
