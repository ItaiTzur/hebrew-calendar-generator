﻿namespace ItaiTzur.utilities.hebrew_calendar_generator
{
    using BizArk.ConsoleApp;
    using ItaiTzur.utilities.hebrew_calendar_generator.Excel;
    using ItaiTzur.utilities.hebrew_calendar_generator.Interfaces;
    using ItaiTzur.utilities.hebrew_calendar_generator.Interfaces.Excel;
    using Microsoft.Extensions.DependencyInjection;
    using System;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.IO;
//    using System.Text;
//    using System.Threading.Tasks;

    internal sealed class Program : BaseConsoleApp
    {
        public Program()
        {
            var today = DateTime.Today;
            var year = today.Year;
            Year = today.Month >= 9 ? year : year - 1;
        }

        private static void Main()
        {
            BaCon.Start<Program>();
        }

        [Required]
        [CmdLineArg("out")]
        [Description("The path of the output Excel calendar.")]
        public string OutPath { get; set; }

        [CmdLineArg("y", ShowInUsage = true)]
        [Description("The year for which the calendar (September to August) is to be created.")]
        public int Year { get; set; }

        [CmdLineArg("rtl", ShowInUsage = true)]
        [Description("'true' for a right-to-left calendar; if omitted, 'false' (left-to-right) is used.")]
        public bool RightToLeft { get; set; }

        [CmdLineArg("l", ShowInUsage = true)]
        [Description("'true' for a large calendar; if omitted and InPath is unused, 'false' (small) is used.")]
        public bool Large { get; set; }

        [CmdLineArg("in", ShowInUsage = true)]
        [Description(@"The path to a personal-events file; creates a large calendar, regardles of the IsLarge argument.
	The first line in this file must contain a font name.
	Each subsequent line must be of the following format:
	 <day><tab char><month><tab char>[<year>]<tab char><event name>
	Examples:
	 11	3	1952	Douglas Adams' birthday
	 25	5		Towel Day
	This file may contain Hebrew dates, but they must appear with Hebrew
	month's name, Hebrew day and optional fully-qualified (5-letters) year.
	Rows beginning with tab characters will be ignored.")]
        public string InPath { get; set; }

        public override int Start()
        {
            IAssemblyOperator assemblyOperator;
            IHebrewCalendarGenerator hebrewCalendarGenerator;
            using (var serviceProvider = SetupDependencyInjection())
            {
                assemblyOperator = serviceProvider.GetService<IAssemblyOperator>();
                hebrewCalendarGenerator = serviceProvider.GetService<IHebrewCalendarGenerator>();
            }

            hebrewCalendarGenerator.Generate(
                OutPath,
                Year,
                Large || InPath != null,
                RightToLeft,
                InPath,
                $"{Path.GetDirectoryName(assemblyOperator.GetExecutingAssembly().Location)}\\HebrewHolidays.yaml");
            return 0;
        }

        private static ServiceProvider SetupDependencyInjection()
        {
            return new ServiceCollection()
                .AddSingleton<IApplicationFactory, ApplicationFactory>()
                .AddSingleton<IAssemblyOperator, AssemblyProvider>()
                .AddSingleton<IConsoleOperator, ConsoleWrapper>()
                .AddSingleton<IDirectoryOperator, DirectoryWrapper>()
                .AddSingleton<IExcelCalendarFactory, ExcelCalendarFactory>()
                .AddSingleton<IFileOperator, FileWrapper>()
                .AddSingleton<IHebrewCalendarGenerator, HebrewCalendarGenerator>()
                .AddSingleton<IHebrewDateCalculator, HebrewDateCalculator>()
                .AddSingleton<IHolidayRepositoryFactory, HolidayRepositoryFactory>()
                .AddSingleton<IPathOperator, PathWrapper>()
                .BuildServiceProvider();
        }
    }
}
