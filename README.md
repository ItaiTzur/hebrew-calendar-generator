# hebrew-calendar-generator
This is a .NET-Framework console application that creates a Hebrew calendar. It receives the following command-line arguments (**boldface** represents a mandatory argument):
| Argument                 | Meaning                                                                                                                        |
| ------------------------ | ------------------------------------------------------------------------------------------------------------------------------ |
| **/out *<OUTPUT PATH>*** | **The path of the output Excel calendar**                                                                                      |
| /y *<CALENDAR YEAR>*     | The year for which the calendar (September to August) is to be created                                                         |
| /rtl                     | A flag for creating a right-to-left calendar                                                                                   |
| /l                       | A flag for creating a large calendar (where every month spans two pages); used automatically if the */in* argument is provided |
| /in *<INPUT PATH>*       | The path to a personal-events file; creates a large calendar, regardles of the */l* argument                                   |
If an *<INPUT PATH>* is provided, the first line in this file must contain a font name to be used for the personal events in this file.

Each subsequent line must be of the following format:
```
 <day><tab character><month><tab character>[<year>]<tab character><event name>
```
Rows beginning with a *Tab* character will be ignored.
## Events on Gregorian Dates
For a personal event on a Gregorian date, the *<day>*, *<month>* and (optional) *<year>* must be integers representing a valid Gregorian date.

Examples:
```
 11     3       1952    Douglas Adams' birthday
 25     5               Towel Day
```
This will create two personal events - *Douglas Adams' birthday* on March 11 (indicating the number of years that have passed since 1952), and *Towel Day* on May 25 (not indicating years).
## אירועים בתאריכים עבריים
כדי ליצור אירוע בתאריך עברי, צריכים היום, החודש והשנה (האופציונלית) לייצג תאריך עברי תקני: היום צריך להופיע ללא מרכאות, החודש צריך להיות שם של חודש עברי והשנה (אם קיימת) צריכה להיות שנה מלאה (כולל תחילית האלָפים) ללא מרכאות.

דוגמא:
```
כו	ניסן	התשיה	יום־השנה של סבא־רבא אלברט
```
שורה כזו תיצור אירוע בשם *יום־השנה של סבא־רבא אלברט* בתאריך כ"ו ניסן (ותציין את מס' השנים שחלפו משנת ה'תשי"ה).
## Usage Example
```
ItaiTzur.utilities.hebrew-calendar-generator.exe /out C:\temp\Calendar2020_2021 /y 2020 /rtl /in C:\temp\MyEvents.txt
```
This will read the *C:\temp\MyEvents.txt* file (which should be of the personal-events file format explained above) and create a (large) right-to-left calendar called *Calendar2020_2021.xlsx* (under the *C:\temp* folder) spanning the year from September 2020 to August 2021 (inclusive).
## Date Conversion
The calculation for the conversion to Hebrew dates can be found at [www.chelm.org](http://www.chelm.org/jewish/calendar/algorithms.html) or [computer-programming-forum.com](http://computer-programming-forum.com/18-clarion/fb6dd59026748002.htm#:~:text=The%20hebrew%20calendar,5757.) or [docs.google.com](https://docs.google.com/document/preview?hgd=1&id=1trlIKgiKIANyGyOR9gXsQagUIPdOeqD2NoTzlxGOZJc).


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:3290f8aa54b4f7977f41596352c9b0c3?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:3290f8aa54b4f7977f41596352c9b0c3?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:3290f8aa54b4f7977f41596352c9b0c3?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/ItaiTzur/hebrew-calendar-generator.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:3290f8aa54b4f7977f41596352c9b0c3?https://gitlab.com/ItaiTzur/hebrew-calendar-generator/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:3290f8aa54b4f7977f41596352c9b0c3?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:3290f8aa54b4f7977f41596352c9b0c3?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:3290f8aa54b4f7977f41596352c9b0c3?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://gitlab.com/-/experiment/new_project_readme_content:3290f8aa54b4f7977f41596352c9b0c3?https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:3290f8aa54b4f7977f41596352c9b0c3?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:3290f8aa54b4f7977f41596352c9b0c3?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:3290f8aa54b4f7977f41596352c9b0c3?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:3290f8aa54b4f7977f41596352c9b0c3?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:3290f8aa54b4f7977f41596352c9b0c3?https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://gitlab.com/-/experiment/new_project_readme_content:3290f8aa54b4f7977f41596352c9b0c3?https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://www.makeareadme.com) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

